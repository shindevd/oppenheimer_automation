*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot

*** Variables ***
${dispense_button}    //*[contains(text(),'Dispense Now')]
${CASHDISPENSED}=   //div[contains(text(),'Cash dispensed')]
${dispense_button_color}=   rgb(220, 53, 69)
${dispense_page}=   /dispense
${dispense_now_text}=       Dispense Now
*** Keywords ***

Validate Button Color
    [Arguments]    ${locator}    ${attribute name}
    ${css}=         Get WebElement    ${locator}
    ${prop_val}=    Call Method       ${css}    value_of_css_property    ${attribute name}
    [Return]     ${prop_val}

Verify Cash Dispensed
    Element Should Be Visible   ${CASHDISPENSED}
    Location Should Contain     /dispense

Validate dispense now button
    ${text}=   get element attribute   ${dispense_button}  text
    Should be equal as strings  ${text}  ${dispense_now_text}
