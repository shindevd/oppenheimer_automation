*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot

*** Variables ***
${base_url}=    http://localhost:8080/
${json_path_Valid} =   ${EXECDIR}${/}..\\Data\\taxrelief.json
${json_path_50}=      ${EXECDIR}${/}..\\Data\\taxreliefless50.json
${json_path_round}=      ${EXECDIR}${/}..\\Data\\taxreliefround.json
${clear_data_url}=     /calculator/rakeDatabase
${roundvalue1}=     500.00
${roundvalue2}=     121.61
*** Keywords ***

Upload Json file
    [Arguments]    ${json_path}
    Create Session  oppensession    ${base_url}
    ${json}  Get Binary File  ${json_path}
    ${headers}=   Create Dictionary    Content-Type=application/json
    ${response}=    POST On Session        oppensession    /calculator/insertMultiple     data=${json}    headers=${headers}
    Log To Console   ${response.text}
    Get Lines Matching Regexp    ${response.text}   Alright

Query Database using Get Request
    Create Session  oppensession    ${base_url}
    ${headers}=   Create Dictionary    Content-Type=application/json
    ${response}=    Get Request        oppensession     calculator/taxRelief
    ${jsonformat}=  set Variable   ${response.json()}
    [Return]        ${jsonformat}

Get masked nat id
    [Arguments]  ${natid}

    ${shortened_natid}=   get substring  ${natid}  0  4
    ${masked_natid}=  Catenate  ${shortened_natid}$$$$$$
    Log To Console     ${masked_natid}
    [Return]   ${masked_natid}

