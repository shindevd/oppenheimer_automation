*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot

*** Variables ***
${base_url}    http://localhost:8080/
${json_path_validmulti}    ${EXECDIR}${/}..\\Data\\insertmultiple.json
${json_path_invalidmulti}    ${EXECDIR}${/}..\\Data\\insertmult_invalid.json
${valid_status_code}=        202
${invalid_status_code}=        500
${refresh_table_button}    //button[text()='Refresh Tax Relief Table']
*** Keywords ***
Insert multiple person data using API
    [Arguments]    ${json_path}     ${status_code}
    Create Session  oppensession    ${base_url}
    ${json}  Get Binary File  ${json_path}
    ${headers}=   Create Dictionary    Content-Type=application/json
    ${response}=    POST On Session        oppensession    /calculator/insertMultiple     data=${json}    headers=${headers}    expected_status=${status_code}
    [Return]     ${response}