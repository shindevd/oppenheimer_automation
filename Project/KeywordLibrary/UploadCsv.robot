*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot

*** Variables ***
${URL}      http://localhost:8080/
${upload_csv_input}        //input[@type='file']
${csv_path_single}     ${EXECDIR}${/}..\\Data\\singleperson.csv
${csv_path_multiple}     ${EXECDIR}${/}..\\Data\\multipleperson.csv
${refresh_table_button}    //button[text()='Refresh Tax Relief Table']
${taxrelief_table}     //*[@id="contents"]/div[2]/table
*** Keywords ***
Upload Csv File with Single Record
    wait until element is enabled  ${upload_csv_input}
    Choose file     ${upload_csv_input}     ${csv_path_single}

Upload Csv File with Multiple Record
    wait until element is enabled  ${upload_csv_input}
    Choose file     ${upload_csv_input}     ${csv_path_multiple}

Get Rows from TaxRelief Table
    ${Rowscount}=   get element count   ${taxrelief_table}/tbody/tr
    [Return]     ${Rowscount}