*** Settings ***
Library    RequestsLibrary
Library    Collections
Library    OperatingSystem
Library    String
Library    JSONLibrary
Library    SeleniumLibrary


*** Variables ***
${base_url}=    http://localhost:8080/
${clear_data_url}=     /calculator/rakeDatabase
${refresh_button}=  //button[text()='Refresh Tax Relief Table']
*** Keywords ***
Launch App
    Open Browser    ${base_url}
Clear Data Base
    Create Session    clssession    ${base_url}
    post on session   clssession  url=${clear_data_url}
Click Button
       [Arguments]    ${app_button}
       Click Element    ${app_button}
Validate records are reflected in UI
    Click Element  ${refresh_button}
    sleep  2s
    page should not contain  No records at the moment

Validate records are not reflected in UI
    Click Element  ${refresh_button}
    sleep  2s
    page should contain  No records at the moment

