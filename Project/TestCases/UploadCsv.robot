*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot
Resource   ../KeywordLibrary/UploadCsv.robot

*** Test Cases ***
Verify CSF File Data with Single Record Added in App
    Clear Data Base
    Launch App
    sleep  5s
    Upload Csv File with Single Record
    Validate records are reflected in UI

Verify CSF File Data with Multiple Record Added in App
    sleep  5s
    ${Rowsbefore}=    Get Rows from TaxRelief Table
    Log To Console   ${Rowsbefore}
    Upload Csv File with Multiple Record
    sleep  5s
    Validate records are reflected in UI
    ${Rowsafter}=    Get Rows from TaxRelief Table
    Log To Console   ${Rowsafter}
    ${Rowsafter} =  Evaluate     ${Rowsbefore} +2
    close browser