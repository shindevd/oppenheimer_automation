*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot
Resource   ../KeywordLibrary/InsertMultiple.robot
*** Test Cases ***
Verify InsertMultiple Person With API with Valid Data
    Launch App
    sleep  5s
    Clear Data Base
    ${responsevalid}=    Insert multiple person data using API   ${json_path_validmulti}      ${valid_status_code}
    Get Lines Matching Regexp    ${responsevalid.text}   Alright
    Validate records are reflected in UI
    Clear Data Base

Verify InsertMultiple Person With API with invalid Data

    ${responseinvalid}=    Insert multiple person data using API   ${json_path_invalidmulti}      ${invalid_status_code}
    Log To Console   ${responseinvalid.text}
    Get Lines Matching Regexp    ${responseinvalid.text}   Internal Server Error
    sleep  5s
    Validate records are not reflected in UI
    Clear Data Base
    close browser