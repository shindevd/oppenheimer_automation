*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot
Resource   ../KeywordLibrary/DespenseTaxRelief.robot
*** Test Cases ***
Valid Login
    Launch App
    ${button_colour}=       Validate Button Color   ${dispense_button}  background-color
    Log To Console   ${button_colour}
    Should be equal as strings   ${button_colour}  ${dispense_button_color}
    Validate dispense now button
    Click Button    ${dispense_button}
    Verify Cash Dispensed
    close browser
