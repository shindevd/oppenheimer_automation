*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot
Resource   ../KeywordLibrary/TaxRelief.robot
*** Test Cases ***
Verify Tax Relief Amount Rounded to 50.00 when less than 50.00
    Clear Data Base
    sleep  5s
    Upload Json file    ${json_path_50}
    ${jsonresponse}=    Query Database using Get Request
    @{response_bonus_amounts}=  get value from json    ${jsonresponse}  $..relief
   FOR  ${tax_relief}  IN  @{response_bonus_amounts}
     Should be equal   ${tax_relief}  50.00
   END

Verify tax relief details are properly rounded off
    Clear Data Base
    Upload Json file        ${json_path_round}
    sleep  5s
    ${jsonresponseround}=       Query Database using Get Request
    ${relief}=    Get Value From Json    ${jsonresponseround}   $..relief
    List Should Contain Value   ${relief}  ${roundvalue1}
    List Should Contain Value   ${relief}  ${roundvalue2}

Verify natid is properly masked.
    ${jsonresponsenatid}=       Query Database using Get Request
    ${natidres}=    Get Value From Json    ${jsonresponsenatid}   $..natid
    FOR  ${nid}  IN  @{natidres}
     Log To Console     ${nid}
    ${maskedid}=    Get masked nat id   ${nid}
    Log To Console     ${maskedid}
     Should be equal   ${nid}  ${maskedid}
    END

