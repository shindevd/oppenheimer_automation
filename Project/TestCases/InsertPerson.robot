*** Settings ***
Resource   ../KeywordLibrary/commonkeywords.robot
Resource   ../KeywordLibrary/InsertPerson.robot
*** Test Cases ***
Verify InsertPerson With API Success with Valid Data
    Clear Data Base
    ${responsevalid}=    Insert person data using API with Single Record  ${json_path_Valid}    ${valid_status_code}
    Log To Console   ${responsevalid.text}
    Get Lines Matching Regexp    ${responsevalid.text}   Alright
    Launch App
    sleep  5s
    Validate records are reflected in UI
    Clear Data Base

Verify InsertPerson API Failes For Missing Field
    Clear Data Base
    ${responseinvalid1}=    Insert person data using API with Single Record      ${json_path_invaliddata}   ${invalid_status_code}
    Log To Console   ${responseinvalid1.text}
    Get Lines Matching Regexp    ${responseinvalid1.text}   Internal Server Error
    sleep  5s
    Validate records are not reflected in UI
    Clear Data Base
    close browser
