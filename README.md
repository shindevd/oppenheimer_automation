# Oppenheimer_Automation

# Prerequisites
Install Python
https://www.python.org/downloads/release/python-380/

Install Robot Framework
command : pip install robotframework

Install Selenium Libraray
command : pip install --upgrade robotframework-seleniumlibrary
https://www.selenium.dev/selenium/docs/api/py/

Install Request Library
pip install robotframework-requests
https://pypi.org/project/robotframework-requests/

# Repository Setup

Clone framework from below location, 
git@gitlab.com:shindevd/oppenheimer_automation.git

Validate installation by running below command_
`python --version`
`robot --version`
`pip --version`

# Test Execution

_You can use below command to execute test cases_

All Tests:
`robot ${EXECDIR}/robot *.robot

Single Test Execution: 
robot ${EXECDIR}/InsertPerson.robot


Framework Structure
`TestCases`
	Robot Test Cases managed under this folder.
`KeywordLibrary`
	Keywords used in Robot Framework test cases are stored  under this folder.
`Data`
	Test Data Is stored under this Folder for respective test cases.

Future	Enhancement
1. Create Common variable\elements file.
2. Create Execution Result Structure.
3. Create Single Button Execution using Jenkins\CI\CD pipeline



